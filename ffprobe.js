import cp from "child_process";
import { execute } from "./common";

function probe(file) {
    return execute(`ffprobe -v quiet -print_format json -show_format -show_streams ${file}`).then(({ stdout, stderr }) => {
        return JSON.parse(stdout);
    });
}

probe("media/Sintel.2010.720p.mkv").then((data) => {
  console.log(JSON.stringify(data, null, "  "));
});
