// TODO: Podemos hacer que el Pipeline sea una cosa que genera líneas de Pipeline
// para gst-launch-1.0 y que esos pipelines sean ejecutados por un objeto GStreamer.
//
// Ejemplo:
//    const pipeline1 = GStreamer.createPipeline("filesrc", { "location": "" }).pipe("decodebin").pipe("vtenc_h264").pipe("filesink", { "location": "" })
//    const pipeline2 = GStreamer.createPipeline("filesrc", { "location": "" }).pipe("decodebin").pipe("vtenc_h264").pipe("filesink", { "location": "" })
//    GStreamer.launch([pipeline1,pipeline2]).on("progress");
//

import cp from "child_process";
import fs from "fs";
import path from "path";
import ProgressBar from "ascii-progress";
import EventEmitter from "events";

class Encoder extends EventEmitter {
  static options(options) {
    let list = [];
    for (const name in options) {
      const value = options[name];
      list.push(`${name}=${value}`);
    }
    return list.join(" ");
  }

  static plugin(plugin, options = null) {
    if (options) {
      return `${plugin} ${Encoder.options(options)}`;
    }
    return plugin;
  }

  static create(plugin = null, options = null) {
    const encoder = new Encoder();
    if (plugin) {
      return encoder.pipe(plugin, options);
    }
    return encoder;
  }

  constructor() {
    super();
    this.pipeline = [];
    this.child = null;
  }

  pipe(plugin, options = {}) {
    this.pipeline.push([plugin, options]);
    return this;
  }

  start() {
    const command = this.toString();
    const child = this.child = cp.exec(command);
    child.on("exit", (exitCode) => {
      if (exitCode > 0) {
        this.emit("error");
      } else {
        this.emit("complete");
      }
    });
    child.stdout.on("data", (chunk) => {
      const matches = chunk.toString("utf8").match(/progressreport([0-9]+)\s+\(((?:[0-9]+):(?:[0-9]+):(?:[0-9]+))\):\s+([0-9]+)\s+\/\s+([0-9]+)\s+seconds\s+\((?:\s*)([0-9]+(?:\,[0-9]+)?)\s+%\)/);
      if (matches) {
        const [fullMatch, id, time, current, total, percentage] = matches;
        this.emit("progress", {
          id: parseInt(id,10),
          time: time,
          current: parseInt(current,10),
          total: parseInt(total,10),
          progress: parseFloat(percentage.replace(",",".")) * 0.01,
          percentage: `${percentage}%`
        });
      }
    });
    return this;
  }

  stop() {
    if (this.child) {
      this.child.removeAllListeners();
      this.child.kill();
      this.child = null;
    }
  }

  toString() {
    return `gst-launch-1.0 -q ${this.pipeline.map(([plugin,options]) => Encoder.plugin(plugin,options)).join(" ! ")}`;
  }
}

const bar = new ProgressBar({
  schema: "[:bar] :percent of media/Sintel.2010.720p.mkv -> media/Sintel.2010.720p.mp4"
});

Encoder.create()
  .pipe("filesrc", { "location": "media/Sintel.2010.720p.mkv" })
  .pipe("decodebin")
  .pipe("progressreport update-freq=1")
  .pipe("vtenc_h264_hw")
  .pipe("filesink", { "location": "media/Sintel.2010.720p.mp4" })
  .on("progress", (e) => {
    bar.update(e.progress);
  })
  .start();
