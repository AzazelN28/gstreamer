import cp from "child_process";
import debug from "debug";

const log = debug("gstreamer:log");
const error = debug("gstreamer:error");

class GStreamerPipeline {
	static seconds(value) {
		return {
			asHours() { return value / 3600; },
			asMinutes() { return value / 60; },
			asSeconds() { return value; },
			asMilliseconds() { return value * 1000; },
			asMicroseconds() { return value * 1000000; },
			asNanoseconds() { return value * 1000000000; }
		};
	}

	static getPluginOptions(options) {
		const pluginOptions = [];
		for (const name in options) {
			const value = options[name];
			pluginOptions.push(`${name}=${value}`);
		}
		return pluginOptions.join(" ");
	}

	static plugin(plugin, options = {}) {
		const pluginOptions = GStreamerPipeline.getPluginOptions(options);
		if (pluginOptions) {
			return `${plugin} ${pluginOptions}`;
		}
		return plugin;
	}

	static create(plugin, options = {}) {
		return (new GStreamerPipeline()).pipe(plugin, options);
	}

	constructor() {
		this.pipeline = [];
	}

	pipe(plugin, options = {}) {
		this.pipeline.push(GStreamerPipeline.plugin(plugin, options));
		return this;
	}

	execute(callback) {
		return cp.exec(this.toString(), callback);
	}

	toString() {
		return `gst-launch-1.0 ${this.pipeline.join(" ! ")}`;
	}
}

const child = GStreamerPipeline
	.create("filesrc", { "location": "media/Sintel.2010.720p.mkv" })
	.pipe("decodebin")
	.pipe("vtenc_h264")
	.pipe("splitmuxsink", {
		"location": "out/video_%d.mp4",
		"muxer": GStreamerPipeline.plugin("mp4mux", { "faststart": true, "streamable": true, "fragment-duration": GStreamerPipeline.seconds(5).asMilliseconds() }),
		"send-request-keyframe": true,
		"max-size-time": GStreamerPipeline.seconds(60).asNanoseconds()
	})
	.execute((err, stdout, stderr) => {
		if (err) {
			console.error(err);
		}
		console.log(stdout);
		console.log(stderr);
	});
