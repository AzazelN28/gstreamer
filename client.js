import WebSocket from "ws";

const ws = new WebSocket("ws://localhost:4000");
ws.on("open", () => {
  console.log("Open!");
});
ws.on("message", (data) => {
  console.log("Message!");
});
