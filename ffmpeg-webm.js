export function manifest(inputs, output) {
  const files = inputs.map((input) => `-f webm_dash_manifest -i ${input}`).join(" ");
  const maps = inputs.map((input, index) => `-map ${index}`);
  const adaptationSets = ;
  return `ffmpeg ${files} -c copy ${maps} -f webm_dash_manifest -adaptation_sets ${adaptationSets} ${output}`;
}

export default {
  manifest
}
