# DASH Content Preparation

## Re-encode

```sh
x264 --output intermediate_2400k.264 --fps 24 --preset slow --bitrate 2400 --vbv-maxrate 4800 --vbv-bufsize 9600 --min-keyint 48 --keyint 48 --scenecut 0 --no-scenecut --pass 1 --video-filter "resize:width=1280,height=720" inputvideo.mkv
```

| Parameter | Explanation |
|---|---|
| --output intermediate_2400k.264 | Specifies the output filename. File extension is .264 as it is a raw H.264/AVC stream. |
| --fps 24 | Specifies the framerate which shall be used, here 24 frames per second. |
| --preset slow | Presets can be used to easily tell x264 if it should try to be fast to enhance compression/quality. Slow is a good default. |
| --bitrate 2400 | The bitrate this representation should achieve in kbps. |
| --vbv-maxrate 4800 | Rule of thumb: set this value to the double of --bitrate. |
| --vbv-bufsize 9600 | Rule of thumb: set this value to the double of --vbv-maxrate. |
| --keyint 96 | Sets the maximum interval between keyframes. This setting is important as we will later split the video into segments and at the beginning of each segment should be a keyframe. Therefore, --keyint should match the desired segment length in seconds mulitplied with the frame rate. Here: 4 seconds * 24 frames/seconds = 96 frames. |
| --min-keyint 96 | Sets the minimum interval between keyframes. See --keyint for more information.We achieve a constant segment length by setting minimum and maximum keyframe interval to the same value and furthermore by disabling scenecut detection with the --no-scenecut parameter. |
| --no-scenecut | Completely disables adaptive keyframe decision. |
| --pass 1 | Only one pass encoding is used. Can be set to 2 to further improve quality, but takes a long time. |
| --video-filter "resize:width=1280,height=720" | Is used to change the resolution. Can be omitted if the resolution should stay the same as in the source video. |
| inputvideo.mkv | The source video |

## Segmenting

```sh
MP4Box -add intermediate_2400k.264 -fps 24 output_2400k.mp4
```

| Parameter | Explanation |
|---|---|
| -add intermediate_2400k.264 | The H.264/AVC raw video we want to put in a mp4. |
| -fps 24 | Specifies the framerate. H.264 doesn’t provide meta information about the framerate so it’s recommended to specify it. The number (in this example 24 frames per second) must match the framerate used in the x264 command. |
| output_2400k.mp4 | The output file name. |

```sh
MP4Box -dash 4000 -frag 4000 -rap -segment-name segment_ output_2400k.mp4
```

| Parameter | Explanation |
|---|---|
| -dash 4000 | Segments the given file into 4000ms chunks.
| -frag 4000 | Creates subsegments within segments and the duration therefore must be longer than the duration given to -dash. By setting it to the same value, there will only one subsegment per segment. Please see refer to this GPAC post for more information on fragmentation, segmentation, splitting and interleaving.
| -rap | Forces segments to start random access points, i.e. keyframes. Segment duration may vary due to where keyframes are in the video – that’s why we (re-) encoded the video before with the appropriate settings!
| -segment-name segment_ | The name of the segments. An increasing number and the file extension is added automatically. So in this case, the segments will be named like this: segment_1.m4s, segment_2.m4s, …
| output_2400k.mp4 | The video we have created just before which should be segmented.


