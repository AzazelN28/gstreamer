

class XML {
  static attributes(attributes) {
    let list = [];
    for (const name in attributes) {
      const value = attributes[name];
      list.push(`${name}="${value}"`);
    }
    return list.join(" ");
  }

  static open(name, attributes = null) {
    if (attributes) {
      return `<${name} ${XML.attributes(attributes)}>`;
    }
    return `<${name}>`;
  }

  static complete(name, attributes = null) {
    if (attributes) {
      return `<${name} ${XML.attributes(attributes)}/>`;
    }
    return `<${name}/>`;
  }

  static close(name) {
    return `</${name}>`;
  }

  static node([name, attributes, children]) {
    if (children) {
      let string = XML.open(name, attributes);
      for (const child of children) {
        if (Array.isArray(child)) {
          string += XML.node(child);
        } else {
          string += child;
        }
      }
      string += XML.close(name);
      return string;
    }
    return XML.complete(name, attributes);
  }
}

/*
const MPD = `<MPD
	xmlns="urn:mpeg:dash:schema:mpd:2011"
	type="dynamic"
	profiles="urn:mpeg:dash:profile:isoff-live:2011,urn:com:dashif:dash264"
	availabilityStartTime="${config.availabilityStartTime}"
	publishTime="${config.publishTime}"
	minBufferTime="PT5S"
suggestedPresentationDelay="PT15S">
	<UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-head:2014" value="https://vm2.dashif.org/dash/time.txt" />
	<Period start="PT00H00M0.00S">
		<AdaptationSet segmentAlignment="true">
			<SegmentTemplate timescale="1000" duration="${seconds(config.fragment.duration).asMilliseconds()}" />
			<Representation id="0" bandwidth="515348" width="${config.videoInfo.tracks[0].sample_descriptions[0].width}" height="${config.videoInfo.tracks[0].sample_descriptions[0].height}" mimeType="video/mp4" codecs="${config.videoInfo.tracks[0].sample_descriptions[0].codecs_string}">
				<SegmentTemplate media="video_$Number$.mp4" startNumber="0" />
			</Representation>
		</AdaptationSet>
	</Period>
</MPD>`;
*/

const Role = {
  CAPTION: "caption"
  SUBTITLE: "subtitle"
  MAIN: "main", // main media component(s) which is/are intended for presentation if no other information is provided
  ALTERNATE: "alternate", // media content component(s) that is/are an alternative to (a) main media content component(s) of the same media component type (see note 2 below)
  SUPPLEMENTARY: "supplementary", // media content component that is supplementary to a media content component of a different media component type (see Note 1 below)
  COMMENTARY: "commentary", // media content component with commentary (e.g. director’s commentary) (typically audio)
  DUB: "dub" // media content component which is presented in a different language from the original (e.g. dubbed audio, translated captions)
}

const Profile = {
  FULL: "urn:mpeg:dash:profile:full:2011", // identifier for Full profile.
  ISOFF_ON_DEMAND: "urn:mpeg:dash:profile:isoff-on-demand:2011", // identifier for ISO Base media file format On Demand profile.
  ISOFF_LIVE: "urn:mpeg:dash:profile:isoff-live:2011", // identifier for ISO Base media file format live profile.
  ISOFF_MAIN: "urn:mpeg:dash:profile:isoff-main:2011", // identifier for ISO Base media file format main profile.
  MP2T_MAIN: "urn:mpeg:dash:profile:mp2t-main:2011", // identifier for MPEG-2 TS main profile.
  MP2T_SIMPLE: "urn:mpeg:dash:profile:mp2t-simple:2011" // identifier for MPEG-2 TS simple profile.
};

class SegmentBase {
  constructor() {
    this.timescale
    this.presentationTimeOffset
    this.indexRange
    this.indexRangeExact = false;
    this.availabilityTimeOffset
    this.availabilityTimeComplete
  }
}

class MultipleSegmentBase extends SegmentBase {
  constructor() {
    this.duration = 0;
    this.startNumber = 0;
  }
}

class SegmentTemplate extends {
  constructor() {
    this.media = ; // optional
    this.index = ; // optional
    this.initialization = ; // optional
    this.bitstreamSwitching = ; // optional
  }
}

class Representation {
  constructor() {
    this.id; // mandatory
    this.bandwidth; // mandatory
    this.qualityRanking; // optional
    this.dependencyId; // optional
    this.mediaStreamStructureId; // optional
  }
}

class AdaptationSet {
  constructor() {
    this.id = undefined; // optional
    this.group = undefined; // optional
    this.lang = undefined; // optional
    this.contentType = undefined; // optional
    this.par =  // optional;
    this.minBandwidth = ;
    this.maxBandwidth = ;
    this.minWidth;
    this.maxWidth;
    this.minHeight;
    this.maxHeight;
    this.minFrameRate;
    this.maxFrameRate;
    this.segmentAlignment = false;
    this.bitstreamSwitching = undefined;
    this.subsegmentAlignment = false;
    this.subsegmentStartsWithSAP = 0;
  }
}

// un MPD contiene uno o más Period
class Period {
  constructor() {
    this.id = undefined; // optional
    this.start = undefined; // optional
    this.duration = undefined; // optional
    this.bitstreamSwitching = false;
  }
}

class MPD {
  constructor() {
    this.id = undefined; // optional
    this.profiles = profiles; // mandatory
    this.minBufferTime = // mandatory
    this.type = "static"; // optional with default value
    this.publishTime; // optional with default value (mandatory for type = "dynamic")
    this.availabilityStartTime; // conditionally mandatory (mandatory for type = "dynamic")
    this.availabilityEndTime; // optional.
    this.timeShiftBufferDepth; // optional (for "dynamic")
    this.maxSegmentDuration; // optional
    this.maxSubsegmentDuration; // optinal
    this.mediaPresentationDuration; // optional
    this.mediaPresentationDuration; // optional
    this.suggestedPresentationDelay; // optional (for "dynamic")
  }
}
