import cp from "child_process";

class Pipeline {
  static options(options) {
    const list = [];
    for (const name in options) {
      const value = options[name];
      list.push(`${name}=${value}`);
    }
    return list.join(" ");
  }

  static plugin(plugin, options) {
    if (options) {
      return `${plugin} ${Pipeline.options(options)}`;
    }
    return plugin;
  }

  constructor() {
    this.pipeline = [];
    this.separator = " ! ";
  }

  pipe(plugin, options) {
    this.pipeline.push([plugin, options]);
    return this;
  }

  toString() {
    return this.pipeline.join(this.separator);
  }
}

export const GStreamer {
  pipeline(plugin, options) {
    return (new Pipeline()).pipe(plugin, options);
  }

  start(pipelines, options, ...args) {
    return cp.exec(`gst-launch-1.0 ${opts} ${pipelines.join(" ")}`, ...args);
  }
}

export default GStreamer;
