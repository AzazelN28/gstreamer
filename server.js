import WebSocket from "ws";
import fs from "fs";
import path from "path";
import util from "util";

const access = util.promisify(fs.access);
const stat = util.promisify(fs.stat);

const MessageType = {
  PROBE: "probe",
  ENCODE: "encode"
};

const wss = new WebSocket.Server({ port: 4000 });
wss.on("connection", (ws) => {
  ws.on("message", (data) => {
    const message = JSON.parse(data.toString("utf8"));
    if (message.type === MessageType.PROBE) {
      stat(message.payload.path).then((stats) => {
        stats.
        // TODO:
      });
    } else if (message.type === MessageType.ENCODE) {

    }
  });
});
