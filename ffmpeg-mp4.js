import { execute, isReadable, getOptions } from "./common";

/**
 * Encodes a video into mp4.
 *
 * @param {String} input
 * @param {String} output
 * @param {Object} options
 * @return {Promise}
 */
export function encode(input, output, options = {}) {
  const opts = getOptions({ preset: "fast", profile: "main" }, options);
  return isReadable(input).then((file) => {
    return execute(`ffmpeg -i ${input} -f mp4 -vcodec libx264 -preset ${opts.preset} -profile:v ${opts.profile} -codec:a aac ${output}`);
  });
}

/**
 * Concats multiple videos into a larger video file.
 *
 * @param {Array<String>} inputs
 * @param {String} output
 * @param {Object} [options={}]
 * @return {Promise}
 */
export function concat(inputs, output, options = {}) {
  const opts = getOptions({ preset: "fast", profile: "main" }, options);
  return Promise.all(inputs.map((file) => isReadable(file))).then(() => {
    return execute(`ffmpeg -i “concat:${inputs.join("|")}” -f mp4 -vcodec libx264 -preset ${opts.preset} -profile:v ${opts.profile} -codec:a aac ${output}`);
  });
}

/**
 * Resizes a video and saves it as an mp4.
 *
 * @param {String} input
 * @param {String} output
 * @param {Object} [options={}]
 * @return {Promise}
 */
export function resize(input, output, options = {}) {
  const opts = getOptions({ preset: "fast", profile: "main", width: 1920, height: 1080 }, options);
  return isReadable(input).then((file) => {
    return execute(`ffmpeg -i ${input} -f mp4 -codec:v libx264 -filter:v scale=${opts.width}:${opts.height} -preset ${opts.preset} -profile:v ${opts.profile} -codec:a aac ${output}`);
  });
}

/**
 * Crops a video and saves it as an mp4.
 *
 * @param {String} input
 * @param {String} output
 * @param {Object} [options={}]
 * @return {Promise}
 */
export function crop(input, output, options = {}) {
  const opts = getOptions({ preset: "fast", profile: "main", width: 1920, height: 1080 }, options);
  return isReadable(input).then((file) => {
    return execute(`ffmpeg -i ${input} -f mp4 -codec:v libx264 -filter:v crop=${opts.width}:${opts.height}:${options.x}:${options.y} -preset ${opts.preset} -profile:v ${opts.profile} -codec:a aac ${output}`);
  });
}

export default {
  encode,
  concat,
  resize,
  crop
}
