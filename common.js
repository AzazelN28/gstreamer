import cp from "child_process";
import fs from "fs";
import assign from "deep-assign";

/**
 * Executes a command and returns a promise with its result.
 *
 * @param {String} command
 * @return {Promise}
 */
export function execute(command) {
  return new Promise((resolve, reject) => {
    cp.exec(command, (err, stdout, stderr) => {
      if (err) {
        return reject(err);
      }
      return resolve({ stdout, stderr });
    });
  });
}

/**
 * Returns if the file is readable or not.
 *
 * @param {String} file
 * @return {Promise}
 */
export function isReadable(file) {
  return new Promise((resolve, reject) => {
    fs.access(file, fs.constants.R_OK, (err) => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
}

/**
 * Returns options 
 *
 * @param {Object} defaultOptions
 * @param {Object} options
 * @return {Object}
 */
export function getOptions(defaultOptions, options) {
  return assign(defaultOptions, options);
}

export default {
  isReadable,
  execute,
  getOptions
}
