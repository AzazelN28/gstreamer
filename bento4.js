import { execute, isReadable } from "./common";

/**
 * Returns MP4 info.
 *
 * @param {String} file
 * @return {Promise}
 */
export function info(file) {
  return isReadable(file).then(() => {
    return execute(`mp4info --format json ${file}`).then(({stdout,stderr}) => {
      return JSON.parse(stdout.toString("utf8"));
    });
  });
}

/**
 * Returns MP4 dump.
 *
 * @param {String} file
 * @param {Number} [verbosity=0]
 * @return {Promise}
 */
export function dump(file, verbosity = 0) {
  return isReadable(file).then(() => {
    return execute(`mp4dump --format json --verbosity ${verbosity} ${file}`).then(({stdout,stderr}) => {
      return JSON.parse(stdout.toString("utf8"));
    });
  });
}

/**
 * 
 */
export function fragment() {

}

/**
 * 
 */
export function compact() {

}

info("out/video_0.mp4").then((info) => {
  console.log(JSON.stringify(info, null, "  "));
});

dump("out/video_0.mp4").then((dump) => {
  console.log(JSON.stringify(info, null, "  "));
});
